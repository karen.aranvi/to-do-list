import ToDoList from "./components/ToDoList";
import './index.css';

function App() {
  return (
    <div className="h-full bg-gray-50 pl-10 pb-10">
        <ToDoList />
    </div>
  );
}

export default App;
