import { useState } from "react"
import TextInput from "./TextInput";
import ListItem from "./ListItem";

export default function ToDoList() {
    const [nombre, setNombre] = useState('')
    const [descripcion, setDescripcion] = useState('')
    const [color, setColor] = useState('')
    const [fecha, setFecha] = useState('')
    const [tareas, setTareas] = useState([])

    const agregarTarea = (e) => {
        const nuevaTarea = { nombre, descripcion, color, fecha }
        setNombre('')
        setDescripcion('')
        setColor('')
        setFecha('')
        setTareas([...tareas, nuevaTarea])
        e.preventDefault()
    }

    const eliminarTarea = (index) => {
        let tareasAux = [...tareas]
        tareasAux.splice(index, 1)
        setTareas(tareasAux)
    }
    
    return(
        <>
            <div>
                <h1 className="pt-10 text-2xl font-bold leading-9 tracking-tight text-gray-900">To Do List App</h1>
            </div>

            <div className="mt-10">
                <TextInput 
                    label={'Nombre de la tarea'}
                    id={'nombre'}
                    type={'text'}
                    value={nombre}
                    change={val => setNombre(val)}
                />
            </div>
            <div className="mt-6">
                <TextInput 
                    label={'Descripción'}
                    id={'descripcion'}
                    type={'text'}
                    value={descripcion}
                    change={val => setDescripcion(val)}
                />
            </div>
            <div className="mt-6">
                <TextInput 
                    label={'Color de la tarea'}
                    id={'color'}
                    type={'text'}
                    value={color}
                    change={val => setColor(val)}
                />
            </div>
            <div className="mt-6">
                <TextInput 
                    label={'Fecha de entrega'}
                    id={'fecha'}
                    type={'date'}
                    value={fecha}
                    change={val => setFecha(val)}
                />
            </div>

            <div>
                <button type="submit" onClick={agregarTarea} className="mt-10 justify-center rounded-md bg-indigo-600 px-3 py-1.5 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600">
                    Agregar tarea
                </button>
            </div>

            <div className="mt-10">
                <ListItem tareas={tareas} eliminarTarea={index => eliminarTarea(index)}/>
            </div>
        </>
    );
}