import { DELETE_ICON } from '../styles/deleteIcon'

export default function ListItem({ tareas, eliminarTarea }) {
    return (
        <>
            {
                tareas.map((tarea, index) => {
                    return <>
                        <div key={index}>
                            <ul className="mt-3">
                                <li className="flex rounded-md mt-6">
                                    <div className="flex w-40 flex-shrink-0 items-center justify-center bg-pink-600 rounded-l-md text-sm font-medium text-white">
                                        {tarea.fecha}
                                    </div>
                                    <div className="flex items-center justify-between truncate rounded-r-md border-b border-r border-t border-gray-200 bg-white">
                                        <div className="flex-1 truncate px-4 py-2 text-sm">
                                            <span className="font-medium text-gray-900 hover:text-gray-600">
                                                {tarea.nombre}
                                            </span>
                                            <p className="text-gray-500">{tarea.descripcion}</p>
                                        </div>
                                        <div>
                                            <button onClick={() => eliminarTarea(index)}>
                                                <img className="h-10 mt-1 ml-10" src={DELETE_ICON} alt="Delete Task" />
                                            </button>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </>
                })
            }
        </>
    )
}
