export default function TextInput({ type, label, id, change, value }) {
    return(
        <>    
            <label className="uppercase block text-sm font-medium text-gray-900">{label}</label>
            <input 
            id={id}
            name={id}
            type={type}
            value={value}
            onChange={(e) => change(e.target.value)}
            className="shadow border-radius-lg px-2 py-1 mt-2 ml-2 border"
            />
        </>
    );
}